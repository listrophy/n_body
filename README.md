# N-Body

A implementation of N-Body simulation using Shoes. **Note**: If you use this software, you must pay attention to the delta-time parameters and change them to suit your particular problem.

## Requirements

* [Shoes](http://shoesrb.com/downloads/), version 3 preferred over 4 due to execution speed. Tested on v3.2.24 r2090 (federales).

## Usage

**Note**: These instructions are for Shoes v3.

### First Time

You probably need to tell Shoes about where installed your gems. If you use rvm:

* Open Shoes.app
* Click "Maintain Shoes"
* Click "Jail Break Gems..."
* Add the following directory: `/Users/<name>/.rvm/gems/<rubyversion>@<gemset>/gems`
* Click "Save JailBreak Values"
* Close the "Maintenance" window

### Running the script

In Shoes, just open the `space.rb` file with, for example, [cmd-O].

## License

License information can be found in `LICENSE`.
