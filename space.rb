require 'forwardable'
require 'matrix'
require 'ruby-units'
require 'ostruct'

module HomogeneousVectoring
  def vec_to_hom(vec)
    Vector[*vec.to_a, 1.0]
  end

  def hom_to_vec(vec)
    Vector.elements(vec.to_a[0...-1], false)
  end
end

class Space
  G = Unit('6.673e-11 m^3 kg^-1 s^-2')
  DTs = [
    Unit('1 min'),
    Unit('1 min'),
    Unit('1 min'),
    Unit('1 min'),
    Unit('5 min'),
    Unit('1 hr'),
    Unit('8 hr'),
    Unit('4 d')
  ]

  attr_accessor :bodies
  def initialize(*bodies)
    @bodies = bodies
  end

  def min_body_distance
    @bodies.combination(2).map {|(a, b)| (a.position - b.position).r}.min
  end

  def dt
    DTs[Math.log10(min_body_distance.convert_to('km').scalar).floor] or DTs.last
  end

  def gravity(body, other)
    offset = other.position - body.position
    offset * G * body.mass * other.mass / offset.r ** 3
  end

  def forces_for_body(body)
    (bodies - [body]).reduce(Vector[0,0]) do |memo, other|
      memo + gravity(body, other)
    end
  end

  def forces_on_bodies
    bodies.map do |body|
      forces = (bodies - [body]).reduce(Vector[0,0]) do |memo, other|
        memo + gravity(body, other)
      end
      OpenStruct.new(body: body, force: forces)
    end
  end

  def update
    the_dt = dt

    # do not combine these maps. all bodies' forces
    # must be computed in full before updating positions
    bodies.map do |body|
      [body, forces_for_body(body)]
    end.map do |(body, force)|
      old_pos = body.position.dup

      accel = force / body.mass
      body.velocity += accel * the_dt
      body.position += body.velocity * the_dt

      [old_pos, body.position]
    end
  end
end

class Window
  include HomogeneousVectoring

  WIDTH = 1200
  HEIGHT = 800
  MIN_RADIUS = 5
  MAX_RADIUS = 100
  INSET_PCT = 0.9

  attr_reader :space, :shoes_properties
  def initialize(space)
    @space = space
    @shoes_properties = { width: WIDTH, height: HEIGHT, title: 'Solar System' }

    max_dist = space.bodies.map(&:position).map(&:r).max
    min_window_dim = [HEIGHT, WIDTH].min

    @translate_matrix = Matrix[[1,0,WIDTH/2.0], [0,1,HEIGHT/2.0], [0,0,1]]

    @shrink_matrix = Matrix[
      [min_window_dim / max_dist / 2.0, Unit('0 km^-1')],
      [Unit('0 km^-1'), min_window_dim / max_dist / 2.0]
    ] * INSET_PCT
  end

  def shrink_to_cs(point)
    @shrink_matrix * point
  end

  def xform(point)
    shrunk = shrink_to_cs(point)
    Vector.elements(hom_to_vec(@translate_matrix * vec_to_hom(shrunk)).to_a.map(&:to_base), false)
  end

  def xform_point(point)
    xform(point)
  end

  def xform_body(body)
    rad = radius_for(body)
    xform(body.position) - Vector[rad, rad]
  end

  def radius_for(body)
    case body.radius.convert_to('km').scalar
    when 0.. 2000 then 5
    when 2001..200000 then 10
    else 40
    end
  end

  def line_args_for_points(start, finish)
    [*xform_point(start), *xform_point(finish)]
  end

  def animate
    movements = space.update
    space.bodies.each_with_index do |body, body_index|
      xformed = xform_body(body)
      body.view.left = xformed[0]
      body.view.top = xformed[1]
    end
    movements
  end
end

sun = OpenStruct.new \
  mass: Unit('1.989e30 kg'),
  radius: Unit('695800 km'),
  position: Vector[Unit('0 km'), Unit('0 km')],
  velocity: Vector[Unit('0 km/s'), Unit('0 km/s')],
  color: :yellow

jupiter = OpenStruct.new \
  mass: Unit('1.898e27 kg'),
  radius: Unit('43411 mi'),
  position: Vector[Unit('0 km'), Unit('816.62e6 km')],
  velocity: Vector[Unit('12.44 km/s'), Unit('0 m/s')],
  color: :brown

comet = OpenStruct.new \
  mass: Unit('1000 kg'),
  radius: Unit('10 m'),
  position: Vector[Unit('400.00e6 km'), Unit('-400.0e6 km')],
  velocity: Vector[Unit('-15.47 km/s'), Unit('2 km/s')],
  color: :white

space = Space.new(sun, jupiter, comet)
window = Window.new(space)

Shoes.app(window.shoes_properties) do

  background black
  space.bodies.each do |body|
    fill send(body.color)
    xformed = window.xform_body(body)
    body.view = oval \
      left: xformed[0],
      top:  xformed[1],
      radius: window.radius_for(body)

  end

  stroke white
  animate(60) do
    window.animate.each do |start, finish|
      line(*window.line_args_for_points(start, finish))
    end
  end
end
